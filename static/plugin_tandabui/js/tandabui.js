// version 2
$.ajaxSetup( { type: 'post', success: null, traditional:true });

var tandabui = new Object();

tandabui.batch = function (data) {

	var tam = data.length;
	var i;
	var func;

	for (i=tam-1; i>=0; i--) {
		var command = data[i][0];
		var args = data[i][1];

		var cbk = eval('tandabui.'+command);
		func = cbk(args, func);
	}

	return func;
}
	
tandabui.reveal = function (data, callback) {
	if (data.selector == undefined)
		return false;

	return function() {
		$(data.selector).slideDown('fast', callback);
	}
}

tandabui.hide = function (data, callback) {
	if (data.selector == undefined)
		return false;


	return function() {
		$(data.selector).slideUp('fast', callback);
	}
}

tandabui.val = function(data, callback) {
	if (data.selector == undefined)
		return false;

	if (data.value == undefined)
		return false;

	return function() {
		obj = $(data.selector);

		try {
		if (obj.get(0).tagName.toLowerCase() == "input")
			obj.val(data.value);
		else if (obj.get(0).tagName.toLowerCase() == "select")
			obj.val(data.value).change();
		else if (obj.get(0).tagName.toLowerCase() == "span")
			obj.text(data.value);
		} catch(err) {
			console.log(data.selector+" is missing");
		}

		if (callback != undefined) callback();
	}
}

tandabui.alert = function(data, callback) {
	if (data.message == undefined)
		return false;

	return function() {
		alert(data.message);
		if (callback != undefined) callback();
	}
}

tandabui.log = function(data, callback) {
	if (data.message == undefined)
		return false;

	return function() {
		console.log(data.message);
		if (callback != undefined) callback();
	}
}

tandabui.click = function(data, callback) {

	if (data.selector == undefined)
		return false;

	if (data.url == undefined)
		return false;

	return function() {
		//required (see 'closure')
		var d = data;

		$(d.selector).unbind('click');

		if (d.form != undefined) {

			if (d.form instanceof Object) {
				$(d.selector).click( function() { $.post(url=d.url, data=d.form);});
			} else if (typeof(d.form) == "string") {
				$(d.selector).click( function() { $.post(url=d.url, data=form_params(d.form));});
			} else {
				console.log('FALHOU '+d.form);
			}

		}
		else  {
			$(d.selector).click( function() {$.post(url=d.url);});
		}

		if (callback != undefined) callback();
	}
}

tandabui.change = function(data, callback) {

	if (data.selector == undefined)
		return false;

	if (data.url == undefined)
		return false;

	return function() {
		//required (see 'closure')
		var d = data;

		$(d.selector).unbind('change');

		$(d.selector).change( function() {$.post(url=d.url, data={selected_value: $(this).val()});});

		if (callback != undefined) callback();
	}
}

function tdb_get_url()
{
	var pcol;
	var u = document.URL;

	pcol = "ws://";
	if (u.substring(0, 4) == "http")
		u = u.substr(7);

	u = u.split(':');

	return pcol + u[0]+":8888";
}

function ws_callback(e) {

	var input = JSON.parse(e.data);
	var command = input.command;
	var data = input.data;

	var cbk = eval('tandabui.'+command);
	cbk(data)();

}

function tdb_connect() {

	if (tandabui == undefined || tandabui.session_id == undefined)
		alert("Session ID not defined.");

	var url = tdb_get_url();
	if (!web2py_comet(url+'/realtime/'+tandabui.session_id, ws_callback))
		alert("This browser does not support websockets.");
}

function flat_ids(obj) {

	var li = [];
	flat(li, obj, '');

	return li;

}

function flat(lista, obj, prefix) {

	if ( (obj instanceof Object) && (Object.keys(obj).length>0) ) { // tem filhos

		var skeys = Object.keys(obj);

		for (var w in skeys) {

			var key = skeys[w];

			var n_prefix = prefix + (prefix.length>0 ? '.':'') + key;

			var subobj = obj[key];

			flat(lista, subobj, n_prefix );
		}

	} else {
		lista.push(prefix);
	}
}

tandabui.populate = function(data, callback) {
/*
dada uma estrutura arbitrária em json, esta é tornada plana e depois são buscados na página campos que contenham id com mesmo valor.
Ex: {"a": {"b": {"c": 5}}} -> id="a.b.c"
*/
	if (data.selector == undefined)
		return false;

	if (data.data == undefined)
		return false;

	return function() {

		var lista = flat_ids(data.data);

		for(var i in lista) {
			var jqid = lista[i].replace(/\./g, '\\.')
			var str_jqobj = data.selector+' [data-name="'+jqid+'"]';
			var jqobj = $(str_jqobj);
			var valor = eval('data.data.'+lista[i]);
			if (valor == null)
				valor="";
			if (typeof(valor) == "string")
				valor = valor.replace(/\n/g,"<br/>");
			if (jqobj.length == 1) {
				if (jqobj[0].tagName.toLowerCase() == "input")
					jqobj.val(valor);
				else if (jqobj[0].tagName.toLowerCase() == "select") {
					jqobj.val(valor);
				} else
					jqobj.html(valor);
			}
		}

		if (callback != undefined) callback();
	}
}

function form_params(chave) {

	var resp_dict = {}

	// aqui filtramos os radios fora e depois adicionamos apenas aqueles marcados.
	var inputs = $(chave+' input[type!="radio"], '+chave+' select, '+chave+' input:radio:checked');


	for(var i=0; i<inputs.length; i++) {
		var field = $(inputs[i]);
		var id = field.attr('name');
		var tipo = field.attr('type');
		var valor = field.val();


		if (tipo == "checkbox")
			valor = field.prop('checked');

		if (typeof(resp_dict[id]) == "string") {
			var tmpval = resp_dict[id];
			resp_dict[id] = [];
			resp_dict[id].push(tmpval)
			resp_dict[id].push(valor);
		} else if (typeof(resp_dict[id]) == "object") {
			resp_dict[id].push(valor);
		} else if (typeof(resp_dict[id]) == "undefined") {
			resp_dict[id]=valor;
		} else {
			console.log("Erro! Objeto inesperado: "+typeof(resp_dict[id]));
		}
	}

//	return $.param(resp_dict, true); // não mais necessário, pois 'tradicion=true' em ajaxSetup
	return resp_dict; 
}

tandabui.fill_select = function(data, callback) {

	if (data.selector == undefined)
		return false;

	if (data.options == undefined)
		return false;

	return function() {
	
		var obj = $(data.selector);
		
		if (obj.length == 0) {
			obj = $('[data-name="'+data.selector+'"]');
		}

		if (obj.length == 0) {
			return;
		}

		obj.empty();
		obj.append('<option value="-">--</option>');
		for (var p in data.options) {
			obj.append('<option value="'+data.options[p][0]+'">'+data.options[p][1]+'</option>');

		}
		if (callback != undefined) callback();
	}
}

tandabui.jquery_cmd = function(data, callback) {

	if (data.command == undefined)
		return false;
	
	if (data.selector == undefined)
		return false;
	
	if (data.html == undefined)
		return false;
	

	return function() {
		if (data.command == "append")
			$(data.selector).append(data.html);
		else if (data.command == "prepend")
			$(data.selector).prepend(data.html);
		else if (data.command == "html")
			$(data.selector).html(data.html);
		if (callback != undefined) {
			callback();
		} 
	}
}

tandabui.load = function(data, callback) {
	if (data.selector == undefined)
		return false;

	if (data.url == undefined)
		return false;

	return function() {
		$(data.selector).load(data.url, callback);
	}
}

tandabui.pureJS = function(data, callback) {
	return function() {
		s=0;
		eval("s = function () { "+data.code+" }");
		s();
		if (callback != undefined) callback();
	}
}
//------------------------------------------------------------------------------------------------
