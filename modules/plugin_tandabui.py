# -*- coding: utf-8 -*-

import json
from gluon.serializers import json as w2p_json
from gluon.contrib.comet_messaging import comet_send
from gluon import current


class Tandabui:

	def __init__(self, session_id):
		self.command_stack = []
		self.transaction = self.send
		self.state = "send"
		self.session_id = session_id
		self.broadcast = False

	def setBroadcast(self):
		self.broadcast = True

	def begin(self):
		self.broadcast = False
		self.transaction = self.stack
		self.state = "stack"
		return self

	def stack(self, command, data):
		self.command_stack += [ (command, data) ]
		return self

	def commit(self):
		if self.broadcast == True:
			group = 'broadcast'
		else:
			group = self.session_id

		if self.command_stack:
			tdb_response = w2p_json({'command': 'batch', 'data': self.command_stack})
			comet_send('http://127.0.0.1:8888', tdb_response, 'chelonia', group)
		self.transaction = self.send
		self.command_stack = []
		self.state = "send"
		self.broadcast = False
		return self

	def send(self, command, data):
		tdb_response = w2p_json({'command': command, 'data': data})
		if self.broadcast == True:
			group = 'broadcast'
		else:
			group = self.session_id

		comet_send('http://127.0.0.1:8888', tdb_response, 'chelonia', group)
		self.broadcast = False
		return self

	def reveal(self, selector):
		self.transaction('reveal', {'selector': selector})
	#	send('reveal', {'selector': selector})
		return self

	def hide(self, selector):
		self.transaction('hide', {'selector': selector})
	#	send('hide', {'selector': selector})
		return self

	def toggle(self, selector):
		self.transaction('toggle', {'selector': selector})
		return self

	def alert(self, message):
		self.transaction('alert', {'message': message})
		return self

	def click(self, selector, url, form=None):
		self.transaction('click', {'selector': selector, 'url': url, 'form': form})
		return self

	def change(self, selector, url):
		self.transaction('change', {'selector': selector, 'url': url})
		return self

	def log(self, message):
		self.transaction('log', {'message': message})
		return self

	def val(self, selector, value):
		self.transaction('val', {'selector': selector, 'value': value})
		return self

	def setValue(self, field_name, value):
		self.transaction('val', {'selector': '[data-name="'+field_name+'"]', 'value': value})
		return self

	def populate(self, selector, data_bag):
		self.transaction('populate', {'selector': selector, 'data': data_bag})
		return self

	def fill_select(self, selector, options):
		self.transaction('fill_select', {'selector': selector, 'options': options})
		return self

	def append(self, selector, html):
		self.transaction('jquery_cmd', {'command': 'append', 'selector': selector, 'html': html})
		return self

	def prepend(self, selector, html):
		self.transaction('jquery_cmd', {'command': 'prepend', 'selector': selector, 'html': html})
		return self

	def html(self, selector, html):
		self.transaction('jquery_cmd', {'command': 'html', 'selector': selector, 'html': html})
		return self

	def load(self, selector, url):
		self.transaction('load', {'selector': selector, 'url': url})
		return self

	def pureJS(self, code):
		self.transaction('pureJS', {'code': code})
		return self

	def switch(self, from_selector, to_selector):
		in_transaction = (self.state == "stack") #if in the middle of a transaction, avoid the commit at the end
		self.begin()
		self.transaction('hide', {'selector': from_selector})
		self.transaction('reveal', {'selector': to_selector})
		if not in_transaction:
			self.commit()
