# -*- coding: utf-8 -*-

from gluon import current
#import modules.plugin_tandabui as tdb
tandabui_import = local_import('plugin_tandabui')
# REQUIRED. If we don't write anything in the session, web2py will,
# for peformance reasons, not store it
session.plugin_tandabui = 'tdb'

tdb = tandabui_import.Tandabui(current.response.session_id)
